package com.marco.dao.mapper;

import com.marco.dao.common.IBaseMapper;
import com.marco.dao.entity.UserEntity;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/4/18 6:37 下午
 */
public interface UserMapper extends IBaseMapper<UserEntity> {
}
