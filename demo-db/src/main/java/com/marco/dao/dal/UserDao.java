package com.marco.dao.dal;

import com.marco.dao.common.CURDCommonDao;
import com.marco.dao.entity.UserEntity;
import com.marco.dao.mapper.UserMapper;
import org.springframework.stereotype.Repository;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/4/18 6:37 下午
 */
@Repository
public class UserDao extends CURDCommonDao<UserMapper, UserEntity> {


}
