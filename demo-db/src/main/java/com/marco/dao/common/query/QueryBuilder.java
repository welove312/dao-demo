package com.marco.dao.common.query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 3:48 下午
 */
public class QueryBuilder<T> implements Serializable {


    private List<Filter> filters=new ArrayList<>();

    private List<Order> orders=new ArrayList<>();

    protected T entity;


    public QueryBuilder(){
    }

    public QueryBuilder(T entity){
        this.entity=entity;
    }

    public List<Filter> getFilters() {
        return filters;
    }

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public QueryBuilder addFilter(Filter filter){
        filters.add(filter);
        return this;
    }

    public QueryBuilder addOrder(Order order){
        orders.add(order);
        return this;
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }

    public boolean clear(){
        try {
            filters.clear();
            orders.clear();
            this.entity=null;
            return true;
        }catch (Exception e){

        }
        return false;
    }

    public static <T> QueryBuilder build(T entity){
        return new QueryBuilder(entity);
    }

    public static QueryBuilder build(){
        return build(null);
    }

}
