package com.marco.dao.common;

import com.baomidou.mybatisplus.core.toolkit.LambdaUtils;
import com.baomidou.mybatisplus.core.toolkit.support.SFunction;
import com.baomidou.mybatisplus.core.toolkit.support.SerializedLambda;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.reflection.property.PropertyNamer;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 10:26 下午
 */
public class PropertyCover {


    private PropertyCover(){

    }

    /**
     * 根据lambda表达式获取字段属性
     * */
    public static String covertProperty(SFunction func){
        SerializedLambda lambda = LambdaUtils.resolve(func);
        String entityProperty = PropertyNamer.methodToProperty(lambda.getImplMethodName());
        if(StringUtils.isBlank(entityProperty)){
            throw new IllegalArgumentException("func is null");
        }
        return humpToLine(entityProperty);
    }

    public static String humpToLine(String str) {
        return str.replaceAll("[A-Z]", "_$0").toLowerCase();
    }
}
