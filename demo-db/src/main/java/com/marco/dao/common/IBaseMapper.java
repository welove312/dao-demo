package com.marco.dao.common;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 2:42 下午
 */
public interface IBaseMapper<T> extends BaseMapper<T> {

}
