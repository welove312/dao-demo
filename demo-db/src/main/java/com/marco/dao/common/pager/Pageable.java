package com.marco.dao.common.pager;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 3:36 下午
 */
public class Pageable<T> extends Page<T> {

    public static final int MAX_PAGE_SIZE=10;

    public Pageable(){

    }

    public Pageable(int current, int size) {
        super(current, size);
    }

    public Pageable<T> convertPage(IPage<T> page){
        this.setTotal(page.getTotal());
        this.setSize(page.getSize());
        this.setCurrent(page.getCurrent());
        this.setSearchCount(page.isSearchCount());
        this.setRecords(page.getRecords());
        return this;
    }

    @Override
    public long getPages() {
        long pages=0L;
        if (this.getSize() == 0) {
            return 0;
        } else {
            pages = this.getTotal() / this.getSize();
            if (this.getTotal() % this.getSize() != 0) {
                ++pages;
            }

            return pages;
        }
    }

    @Override
    public boolean hasNext() {
        return this.getCurrent() < this.getPages();
    }
    
    public long getStart(){
    	return (getCurrent()-1)*getSize();
    }
}
