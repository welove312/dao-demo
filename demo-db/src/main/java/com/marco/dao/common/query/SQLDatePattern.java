package com.marco.dao.common.query;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 3:48 下午
 */
public enum SQLDatePattern {

    YYYY_MM_DD("%Y-%m-%d"),
    YYYYMMDD("%Y%m%d");

    private String pattern;

    private SQLDatePattern(String pattern){
        this.pattern=pattern;
    }

    public String getPattern(){
        return this.pattern;
    }
}
