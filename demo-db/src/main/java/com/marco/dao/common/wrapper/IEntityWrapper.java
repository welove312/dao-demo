package com.marco.dao.common.wrapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 3:36 下午
 */
public class IEntityWrapper<T> extends QueryWrapper<T> {

    public void limit(int limit){
        this.last(String.format("limit %s",limit));
    }

    public void limit(int start,int limit){
        this.last(String.format("limit %s,%s",start,limit));
    }



}
