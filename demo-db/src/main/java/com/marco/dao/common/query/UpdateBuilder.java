package com.marco.dao.common.query;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 3:48 下午
 */
public class UpdateBuilder<T> extends QueryBuilder<T>{

	private UpdateBuilder(){

	}
	
    public UpdateBuilder(T updateEntity){
        super();
        this.entity=updateEntity;
    }
}
