package com.marco.dao.common;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/3/3 3:25 下午
 */
@Setter
@Getter
public class BaseEntity<T extends Long> {

    @TableId(value="id", type= IdType.AUTO)
    private Long id;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(update = "now()")
    private Date updateTime;
}
