package com.marco.dao.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.marco.dao.common.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @Author zixiaojun
 * @Description
 * @Date 2021/4/18 6:35 下午
 */
@Data
@TableName("t_user_main")
public class UserEntity extends BaseEntity<Long> {

    private Long accountId;

    private String userUuid;

    private String userName;

    private String avatar;

    private Integer level;

    private Integer sex;

    private String birthDay;
}
