package com.marco.dao;

import com.alibaba.fastjson.JSON;
import com.marco.dao.common.query.Filter;
import com.marco.dao.common.query.Order;
import com.marco.dao.common.query.QueryBuilder;
import com.marco.dao.dal.UserDao;
import com.marco.dao.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
class DemoAppTests {

    @Autowired
    private UserDao userDao;

    @Test
    void contextLoads() {
        QueryBuilder queryBuilder = new QueryBuilder();
        //ueryBuilder.addFilter(Filter.eq(UserEntity::getId,6L));
        //queryBuilder.addFilter(Filter.eq(UserEntity::getUserUuid,"5182577"));
        queryBuilder.addFilter(Filter.like(UserEntity::getUserName,"小猪"));
        queryBuilder.addOrder(Order.desc(UserEntity::getId));
        List<UserEntity> userEntityList = userDao.selectEntities(queryBuilder);

        log.info("userEntityList:{}", JSON.toJSONString(userEntityList));
    }

}
